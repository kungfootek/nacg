# NACG
NGINX and Apache22, Apache24 Config Generator

Creates configurations from a database for:

- nginx
- apache 2.2
- apache 2.4

Roadmap:

1. Create Config Generator as MVP.  [ Done ]
2. Setup PUPPET Templates.
3. Setup CHEF Templates.
2. Drupal Admin Module ( De/Activate Site, List of Active Servers, Etc. ) ( For Tier one support. )
3. Drupal Module for One-click installs for Sales.
4. Drupal Module for Client Self Signup and Payment.


Expand Into:

1. AWS Deployments
2. Modules for other CMS systems.

