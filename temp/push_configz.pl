#!/usr/bin/perl -w

# DB Stuff
use DBI;
use DBD::mysql;
use File::stat;
use feature ':5.10';

## Set the DB Connection
my $dbh_my = DBI->connect("DBI:mysql:dbname=nagc;host=mysql.kungfootek.net", "kftdbdan", "n4dbdtdk", {'RaiseError' => 1});

## Loop for Domainz ##
my $DomainLoopQuery="Select ProjectName, ServerName, ZennCluster from nagc.Domainz";
my $sth_DomainLoopQuery = $dbh_my->prepare($DomainLoopQuery); $sth_DomainLoopQuery->execute();
	while (my $DomainLoopQueryRezult = $sth_DomainLoopQuery->fetchrow_hashref()) {
			my $SubDomainLoopQuery = "Select ServerAlias from SubDomainz where ServerName = '$DomainLoopQueryRezult->{'ServerName'}'";
			my $sth_SubDomainLoopQuery = $dbh_my->prepare($SubDomainLoopQuery); $sth_SubDomainLoopQuery->execute();
			while (my $SubDomainLoopQueryRezults = $sth_SubDomainLoopQuery->fetchrow_arrayref()) {
				$erverAliases = join (", ", @{$SubDomainLoopQueryRezults}); 							#### TODO This must be 'FOREACH'
				$ServerNameString=$DomainLoopQueryRezult->{'ServerName'}." ".$erverAliases."\;";
				$DocumentRoot = $DomainLoopQueryRezult->{'ProjectName'}."-".$DomainLoopQueryRezult->{'ServerName'};
			}

## NGINX
	open (NGINX, "+>", "./outputz/nginx/".$DomainLoopQueryRezult->{'ProjectName'}."-".$DomainLoopQueryRezult->{'ServerName'}.".conf") || die "No Output File";
	open (NGXTEMPLATE, "./templates/nginx.tmpl") || die "Template-Open failure";
	while ($Process=<NGXTEMPLATE>) {
		$Process =~ s/PROJECTNAME/$DomainLoopQueryRezult->{'ProjectName'}/g; 
		$Process =~ s/SERVRNAME/$DomainLoopQueryRezult->{'ServerName'}/g;
		$Process =~ s/SERVERNAMESTRING/$ServerNameString/g;
		$Process =~ s/ZENNCLUSTER/$DomainLoopQueryRezult->{'ZennCluster'}/g;
		print NGINX $Process;
#		print $Process;
	}
		close NGXTEMPLATE;
		close NGINX;

## APACHE 2.2
## ZEN6
	open (ZENSIX, "+>", "./outputz/zen6/".$DomainLoopQueryRezult->{'ProjectName'}."-".$DomainLoopQueryRezult->{'ServerName'}.".conf") || die "No Output File";
	open (ZENSIXTEMPLATE, "./templates/zen6.tmpl") || die "Template-Open failure";
	while ($Process=<ZENSIXTEMPLATE>) {
		$Process =~ s/PROJECTNAME/$DomainLoopQueryRezult->{'ProjectName'}/g; 
		$Process =~ s/SERVERNAME/$DomainLoopQueryRezult->{'ServerName'}/g;
		$Process =~ s/DOCUMENTROOT/$DocumentRoot/g;
		$Process =~ s/SERVERALIAS/$erverAliases/g;
		$Process =~ s/ZENNCLUSTER/$DomainLoopQueryRezult->{'ZennCluster'}/g;
		print ZENSIX $Process;
#		print $Process;
	}
		close ZENSIXTEMPLATE; 
		close ZENSIX;

## APACHE 2.4
## ZEN7
	open (ZENSEVEN, "+>", "./outputz/zen7/".$DomainLoopQueryRezult->{'ProjectName'}."-".$DomainLoopQueryRezult->{'ServerName'}.".conf") || die "No Output File";
	open (ZENSEVENTEMPLATE, "./templates/zen7.tmpl") || die "Template-Open failure";
	while ($Process=<ZENSEVENTEMPLATE>) {
		$Process =~ s/PROJECTNAME/$DomainLoopQueryRezult->{'ProjectName'}/g; 
		$Process =~ s/SERVERNAME/$DomainLoopQueryRezult->{'ServerName'}/g;
		$Process =~ s/DOCUMENTROOT/$DocumentRoot/g;
		$Process =~ s/SERVERALIAS/$erverAliases/g;
		$Process =~ s/ZENNCLUSTER/$DomainLoopQueryRezult->{'ZennCluster'}/g;
		print ZENSEVEN $Process;
#		print $Process;
	}
		close ZENSEVENTEMPLATE; 
		close ZENSEVEN;
} # Finishes Main Loop.